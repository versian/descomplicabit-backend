﻿using DescomplicaBit.Domain.Enums;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DescomplicaBit.Domain.Entities
{
    public class Transaction : EntityBase<int>
    {
        [Required]
        public Cryptocurrency Cryptocurrency { get; set; }

        [Required]
        public OperationType OperationType { get; set; }

        [Required]
        public OperationStatus OperationStatus { get; set; }

        [Required]
        [MaxLength(1024)]
        public string OperationCode { get; set; }

        [Required]
        public decimal Value { get; set; }

        [Required]
        [ForeignKey(nameof(UserFrom))]
        public string UserFromID { get; set; }

        [ForeignKey(nameof(UserTo))]
        public string UserToID { get; set; }

        public string TransactionDescription { get; set; }

        public DateTime? ExpirationDate { get; set; }


        public virtual User UserFrom { get; set; }

        public virtual User UserTo { get; set; }

    }
}
