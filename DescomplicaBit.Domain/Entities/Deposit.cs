﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DescomplicaBit.Domain.Entities
{
    public class Deposit : EntityBase<int>
    {
        [Required]
        [MaxLength(1024)]
        public string AddressToDeposit { get; set; }

        [Required]
        [ForeignKey(nameof(Wallet))]
        public string WalletID { get; set; }

        public DateTime? ExpirationDate { get; set; }


        public virtual Wallet Wallet { get; set; }
        
        public virtual List<DepositHistory> DepositHistory { get; set; }
    }
}
