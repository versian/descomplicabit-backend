﻿using DescomplicaBit.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DescomplicaBit.Domain.Entities
{
    public class Transfer : EntityBase<int>
    {
        [Required]
        public Cryptocurrency Cryptocurrency { get; set; }

        [Required]
        public TransferType TransferType { get; set; }

        [Required]
        public TransferStatus Status { get; set; }

        [Required]
        [ForeignKey(nameof(UserFrom))]
        public string UserFromID { get; set; }
        
        [ForeignKey(nameof(WalletTo))]
        public string WalletToID { get; set; }

        [Required]
        public decimal Value { get; set; }

        [MaxLength(1024)]
        public string ExternalWallet { get; set; }

        public string TransferID { get; set; }

        public DateTime? ScheduledDate { get; set; }


        public virtual User UserFrom { get; set; }
        public virtual Wallet WalletTo { get; set; }
    }
}
