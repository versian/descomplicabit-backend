﻿using DescomplicaBit.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescomplicaBit.Domain.Entities
{
    public class DepositHistory : EntityBase<int>
    {
        [ForeignKey(nameof(Deposit))]
        public int DepositID { get; set; }

        public DepositStatus Status { get; set; }

        public decimal Value { get; set; }


        public virtual Deposit Deposit { get; set; }
    }
}
