﻿using DescomplicaBit.Domain.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DescomplicaBit.Domain.Entities
{
    public class Wallet : EntityBase<string>
    {
        public Wallet()
        {
            ID = Math.Abs(DateTime.Now.Ticks.GetHashCode()).ToString("#-##").PadLeft(11, '1');
        }

        [Required]
        public Cryptocurrency Cryptocurrency { get; set; }

        [MaxLength(1024)]
        public string Address { get; set; }

        [Required]
        public decimal Balance { get; set; }

        [Required]
        [ForeignKey(nameof(User))]
        public string UserID { get; set; }


        public User User { get; set; }
        public List<Transfer> Transfers { get; set; }
    }
}
