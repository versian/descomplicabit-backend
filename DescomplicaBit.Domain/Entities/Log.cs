﻿using DescomplicaBit.Domain.Enums;
using System.ComponentModel.DataAnnotations.Schema;

namespace DescomplicaBit.Domain.Entities
{
    public class Log : EntityBase<int>
    {
        public int? LogParentID { get; set; }

        public LogType LogType { get; set; }

        public LogTitle LogTitle { get; set; }

        public string Description { get; set; }


        [ForeignKey(nameof(LogParentID))]
        public virtual Log LogParent { get; set; }
    }
}
