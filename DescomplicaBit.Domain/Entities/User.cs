﻿using DescomplicaBit.Domain.Enums;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DescomplicaBit.Domain.Entities
{
    public class User : IdentityUser
    {
        [Required]
        public string ConfirmationPassword { get; set; }

        [Required]
        public string CPF { get; set; }

        [Required]
        public string RG { get; set; }

        [Required]
        public string CEP { get; set; }

        [Required]
        public DateTime? Birthdate { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        public string AddressNumber { get; set; }

        [Required]
        public string NeighborHood { get; set; }

        public string Complement { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string AccountID { get; set; }

        [Required]
        public UserLevel Level { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime JoinDate { get; set; }


        public virtual List<Transfer> Transfers { get; set; }
        public virtual List<Wallet> Wallets { get; set; }
    }
}
