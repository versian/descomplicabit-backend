﻿using DescomplicaBit.Domain.Entities;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Threading.Tasks;

namespace DescomplicaBit.Domain.Context
{
    public class DescomplicaBitContext : IdentityDbContext
    {

        public DescomplicaBitContext()
            : base("DescomplicaBitConnection")
        {
            Database.SetInitializer<DescomplicaBitContext>(null);
            Configuration.LazyLoadingEnabled = false;
            Configuration.ProxyCreationEnabled = false;
        }


        #region DbSets

        public DbSet<User> User { get; set; }
        public DbSet<Transfer> Transfer { get; set; }
        public DbSet<Transaction> Transaction { get; set; }
        public DbSet<Wallet> Wallet { get; set; }
        public DbSet<Deposit> Deposit { get; set; }
        public DbSet<DepositHistory> DepositHistory { get; set; }
        public DbSet<Log> Log { get; set; }

        #endregion

        #region Methods

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Postgre
            //modelBuilder.HasDefaultSchema("public");

            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<IdentityUser>().ToTable("User");
            modelBuilder.Entity<IdentityRole>().ToTable("Role");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRole");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaim");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogin");

            /********************************************
             *            Configurações Globais
             ********************************************/

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties()
                .Where(p => p.Name == "ID")
                .Configure(p => p.IsKey().HasColumnOrder(0));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(100));

            modelBuilder.Properties()
                .Where(p => p.Name == "ModificationDate")
                .Configure(p => p.IsOptional());

            modelBuilder.Properties()
                .Where(p => p.Name == "CreationDate")
                .Configure(p => p.IsRequired());

            modelBuilder.Properties()
                .Where(p => p.Name == "Active")
                .Configure(p => p.IsRequired());

            modelBuilder.Entity<Transfer>()
                .Property(x => x.Value)
                .HasPrecision(precision: 30, scale: 20);

            modelBuilder.Entity<Transaction>()
                .Property(x => x.Value)
                .HasPrecision(precision: 30, scale: 20);

            modelBuilder.Entity<Wallet>()
                .Property(x => x.Balance)
                .HasPrecision(precision: 30, scale: 20);

            modelBuilder.Entity<Wallet>()
                .Property(x => x.Balance)
                .HasPrecision(precision: 30, scale: 20);

            modelBuilder.Entity<DepositHistory>()
                .Property(x => x.Value)
                .HasPrecision(precision: 30, scale: 20);

            /********************************************
             *         Fim Configurações Globais
             ********************************************/
        }

        public override int SaveChanges()
        {

            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("CreationDate") != null))
            {

                if (entry.State == EntityState.Added)
                {
                    entry.Property("CreationDate").CurrentValue = DateTime.Now;
                    if (entry.Entity.GetType().GetProperty("Active") != null) entry.Property("Active").CurrentValue = true;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("CreationDate").IsModified = false;
                    if (entry.Entity.GetType().GetProperty("ModificationDate") != null) entry.Property("ModificationDate").CurrentValue = DateTime.Now;
                }

            }


            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("CreationDate") != null))
            {

                if (entry.State == EntityState.Added)
                {
                    entry.Property("CreationDate").CurrentValue = DateTime.Now;
                    if (entry.Entity.GetType().GetProperty("Active") != null) entry.Property("Active").CurrentValue = true;
                }

                if (entry.State == EntityState.Modified)
                {
                    entry.Property("CreationDate").IsModified = false;
                    if (entry.Entity.GetType().GetProperty("ModificationDate") != null) entry.Property("ModificationDate").CurrentValue = DateTime.Now;
                }

            }

            return await base.SaveChangesAsync();
        }

        #endregion

    }
}
