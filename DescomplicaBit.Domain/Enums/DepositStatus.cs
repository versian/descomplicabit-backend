﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescomplicaBit.Domain.Enums
{
    public enum DepositStatus
    {
        PROCESSING,
        COMPLETED,
        ERROR
    }
}
