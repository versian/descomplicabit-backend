﻿namespace DescomplicaBit.Domain.Enums
{
    public enum Status
    {
        WAITING,
        SUCCESS,
        ERROR
    }
}
