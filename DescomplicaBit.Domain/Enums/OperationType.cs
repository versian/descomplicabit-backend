﻿namespace DescomplicaBit.Domain.Enums
{
    public enum OperationType
    {
        QR_CODE,
        FAST_CODE,
        DIRECT_OPERATION
    }
}
