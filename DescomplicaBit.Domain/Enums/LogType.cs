﻿namespace DescomplicaBit.Domain.Enums
{
    public enum LogType
    {
        NEUTRAL,
        SUCCESS,
        WARNING,
        ERROR
    }
}
