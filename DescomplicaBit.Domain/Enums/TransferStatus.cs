﻿namespace DescomplicaBit.Domain.Enums
{
    public enum TransferStatus
    {
        SCHEDULED,
        STARTED,
        SUCCESS,
        ERROR,
        CANCELED
    }
}
