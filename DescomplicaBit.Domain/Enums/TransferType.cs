﻿namespace DescomplicaBit.Domain.Enums
{
    public enum TransferType
    {
        EXTERNAL_WALLET,
        INTERNAL
    }
}
