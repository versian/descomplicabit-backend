﻿namespace DescomplicaBit.Domain.Enums
{
    public enum MessageType
    {
        ALERT,
        ERROR,
        SUCCESS
    }
}
