﻿namespace DescomplicaBit.Domain.Enums
{
    public enum UserLevel
    {
        ADMINISTRATOR,
        COMMON_USER
    }
}
