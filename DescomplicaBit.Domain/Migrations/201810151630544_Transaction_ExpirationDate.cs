namespace DescomplicaBit.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Transaction_ExpirationDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transaction", "ExpirationDate", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transaction", "ExpirationDate");
        }
    }
}
