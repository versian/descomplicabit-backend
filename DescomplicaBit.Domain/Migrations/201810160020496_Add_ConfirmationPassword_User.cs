namespace DescomplicaBit.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ConfirmationPassword_User : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.User", "ConfirmationPassword", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("dbo.User", "ConfirmationPassword");
        }
    }
}
