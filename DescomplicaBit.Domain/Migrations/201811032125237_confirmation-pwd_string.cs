namespace DescomplicaBit.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class confirmationpwd_string : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Deposit", "PrivateKey", c => c.String(nullable: false, maxLength: 1024, unicode: false));
            AlterColumn("dbo.User", "ConfirmationPassword", c => c.String(maxLength: 100, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.User", "ConfirmationPassword", c => c.Int());
            DropColumn("dbo.Deposit", "PrivateKey");
        }
    }
}
