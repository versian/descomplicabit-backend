namespace DescomplicaBit.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Tbl_Transfer_Add_TransferID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Transfer", "TransferID", c => c.String(maxLength: 500, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Transfer", "TransferID");
        }
    }
}
