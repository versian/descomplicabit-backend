namespace DescomplicaBit.Domain.Migrations
{
    using DescomplicaBit.Domain.Context;
    using DescomplicaBit.Domain.Entities;
    using DescomplicaBit.Domain.Enums;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity.Migrations;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<DescomplicaBitContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DescomplicaBitContext context)
        {
            //if (System.Diagnostics.Debugger.IsAttached == false)
            //    System.Diagnostics.Debugger.Launch();

            using (var manager = new UserManager<User>(new UserStore<User>(context)))
            {
                var appUser = new User
                {
                    UserName = "Admin",
                    Level = UserLevel.ADMINISTRATOR,
                    JoinDate = DateTime.Now,
                    Email = "contato@versian.com.br",
                    CPF = "999.999.999-99",
                    RG = "99.999.999-9",
                    CEP = "99999-999",
                    Address = "Avenue 1",
                    AddressNumber = "999",
                    NeighborHood = "NeighborHood 1",
                    City = "City 1",
                    State = "State 1",
                    Birthdate = DateTime.Parse("13/01/1997"),
                    AccountID = DateTime.Now.Ticks.GetHashCode().ToString("#######-##"),
                    ConfirmationPassword = ""
            };

                appUser.Claims.Add(new IdentityUserClaim
                {
                    ClaimType = nameof(UserLevel),
                    ClaimValue = UserLevel.ADMINISTRATOR.ToString(),
                    UserId = appUser.Id
                });

                manager.Create(appUser, Encoding.UTF8.GetString(Convert.FromBase64String("RGVzY29tcGxpY2FCaXRAMTIz")));
            }
        }
    }
}
