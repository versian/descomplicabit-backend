namespace DescomplicaBit.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Add_ConfirmationPassword_User_NotNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.User", "ConfirmationPassword", c => c.Int(nullable: false));
        }

        public override void Down()
        {
            AlterColumn("dbo.User", "ConfirmationPassword", c => c.Int());
        }
    }
}
