namespace DescomplicaBit.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class adjust_models_precision : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DepositHistory", "Value", c => c.Decimal(nullable: false, precision: 30, scale: 20));
            AlterColumn("dbo.Wallet", "Balance", c => c.Decimal(nullable: false, precision: 30, scale: 20));
            AlterColumn("dbo.Transfer", "Value", c => c.Decimal(nullable: false, precision: 30, scale: 20));
            AlterColumn("dbo.Transaction", "Value", c => c.Decimal(nullable: false, precision: 30, scale: 20));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Transaction", "Value", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Transfer", "Value", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Wallet", "Balance", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.DepositHistory", "Value", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
