namespace DescomplicaBit.Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Remove_PrivateKey_Deposit : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Deposit", "PrivateKey");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deposit", "PrivateKey", c => c.String(nullable: false, maxLength: 1024, unicode: false));
        }
    }
}
