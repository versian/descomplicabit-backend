namespace DescomplicaBit.Domain.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Deposit",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    AddressToDeposit = c.String(nullable: false, maxLength: 1024, unicode: false),
                    WalletID = c.String(nullable: false, maxLength: 100, unicode: false),
                    ExpirationDate = c.DateTime(),
                    CreationDate = c.DateTime(nullable: false),
                    ModificationDate = c.DateTime(),
                    Active = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Wallet", t => t.WalletID)
                .Index(t => t.WalletID);

            CreateTable(
                "dbo.DepositHistory",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    DepositID = c.Int(nullable: false),
                    Status = c.Int(nullable: false),
                    Value = c.Decimal(nullable: false, precision: 30, scale: 20),
                    CreationDate = c.DateTime(nullable: false),
                    ModificationDate = c.DateTime(),
                    Active = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Deposit", t => t.DepositID)
                .Index(t => t.DepositID);

            CreateTable(
                "dbo.Wallet",
                c => new
                {
                    ID = c.String(nullable: false, maxLength: 100, unicode: false),
                    Cryptocurrency = c.Int(nullable: false),
                    Address = c.String(maxLength: 1024, unicode: false),
                    Balance = c.Decimal(nullable: false, precision: 30, scale: 20),
                    UserID = c.String(nullable: false, maxLength: 100, unicode: false),
                    CreationDate = c.DateTime(nullable: false),
                    ModificationDate = c.DateTime(),
                    Active = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.UserID)
                .Index(t => t.UserID);

            CreateTable(
                "dbo.Transfer",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    Cryptocurrency = c.Int(nullable: false),
                    TransferType = c.Int(nullable: false),
                    Status = c.Int(nullable: false),
                    UserFromID = c.String(nullable: false, maxLength: 100, unicode: false),
                    WalletToID = c.String(maxLength: 100, unicode: false),
                    Value = c.Decimal(nullable: false, precision: 30, scale: 20),
                    ExternalWallet = c.String(maxLength: 1024, unicode: false),
                    ScheduledDate = c.DateTime(),
                    CreationDate = c.DateTime(nullable: false),
                    ModificationDate = c.DateTime(),
                    Active = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.UserFromID)
                .ForeignKey("dbo.Wallet", t => t.WalletToID)
                .Index(t => t.UserFromID)
                .Index(t => t.WalletToID);

            CreateTable(
                "dbo.User",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 100, unicode: false),
                    Email = c.String(maxLength: 256, unicode: false),
                    EmailConfirmed = c.Boolean(nullable: false),
                    PasswordHash = c.String(maxLength: 100, unicode: false),
                    SecurityStamp = c.String(maxLength: 100, unicode: false),
                    PhoneNumber = c.String(maxLength: 100, unicode: false),
                    PhoneNumberConfirmed = c.Boolean(nullable: false),
                    TwoFactorEnabled = c.Boolean(nullable: false),
                    LockoutEndDateUtc = c.DateTime(),
                    LockoutEnabled = c.Boolean(nullable: false),
                    AccessFailedCount = c.Int(nullable: false),
                    UserName = c.String(nullable: false, maxLength: 256, unicode: false),
                    CPF = c.String(maxLength: 100, unicode: false),
                    RG = c.String(maxLength: 100, unicode: false),
                    CEP = c.String(maxLength: 100, unicode: false),
                    Birthdate = c.DateTime(),
                    Address = c.String(maxLength: 100, unicode: false),
                    AddressNumber = c.String(maxLength: 100, unicode: false),
                    NeighborHood = c.String(maxLength: 100, unicode: false),
                    Complement = c.String(maxLength: 100, unicode: false),
                    City = c.String(maxLength: 100, unicode: false),
                    State = c.String(maxLength: 100, unicode: false),
                    AccountID = c.String(maxLength: 100, unicode: false),
                    Level = c.Int(),
                    JoinDate = c.DateTime(),
                    Discriminator = c.String(nullable: false, maxLength: 128),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");

            CreateTable(
                "dbo.UserClaim",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    UserId = c.String(nullable: false, maxLength: 100, unicode: false),
                    ClaimType = c.String(maxLength: 100, unicode: false),
                    ClaimValue = c.String(maxLength: 100, unicode: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.UserLogin",
                c => new
                {
                    LoginProvider = c.String(nullable: false, maxLength: 100, unicode: false),
                    ProviderKey = c.String(nullable: false, maxLength: 100, unicode: false),
                    UserId = c.String(nullable: false, maxLength: 100, unicode: false),
                })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId);

            CreateTable(
                "dbo.UserRole",
                c => new
                {
                    UserId = c.String(nullable: false, maxLength: 100, unicode: false),
                    RoleId = c.String(nullable: false, maxLength: 100, unicode: false),
                })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.Role", t => t.RoleId)
                .ForeignKey("dbo.User", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);

            CreateTable(
                "dbo.Log",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    LogParentID = c.Int(),
                    LogType = c.Int(nullable: false),
                    LogTitle = c.Int(nullable: false),
                    Description = c.String(maxLength: 100, unicode: false),
                    CreationDate = c.DateTime(nullable: false),
                    ModificationDate = c.DateTime(),
                    Active = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Log", t => t.LogParentID)
                .Index(t => t.LogParentID);

            CreateTable(
                "dbo.Role",
                c => new
                {
                    Id = c.String(nullable: false, maxLength: 100, unicode: false),
                    Name = c.String(nullable: false, maxLength: 256, unicode: false),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");

            CreateTable(
                "dbo.Transaction",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    Cryptocurrency = c.Int(nullable: false),
                    OperationType = c.Int(nullable: false),
                    OperationStatus = c.Int(nullable: false),
                    OperationCode = c.String(nullable: false, maxLength: 1024, unicode: false),
                    Value = c.Decimal(nullable: false, precision: 30, scale: 20),
                    UserFromID = c.String(nullable: false, maxLength: 100, unicode: false),
                    UserToID = c.String(maxLength: 100, unicode: false),
                    TransactionDescription = c.String(maxLength: 100, unicode: false),
                    CreationDate = c.DateTime(nullable: false),
                    ModificationDate = c.DateTime(),
                    Active = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.User", t => t.UserFromID)
                .ForeignKey("dbo.User", t => t.UserToID)
                .Index(t => t.UserFromID)
                .Index(t => t.UserToID);

        }

        public override void Down()
        {
            DropForeignKey("dbo.UserRole", "UserId", "dbo.User");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.User");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.User");
            DropForeignKey("dbo.Transaction", "UserToID", "dbo.User");
            DropForeignKey("dbo.Transaction", "UserFromID", "dbo.User");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.Log", "LogParentID", "dbo.Log");
            DropForeignKey("dbo.Deposit", "WalletID", "dbo.Wallet");
            DropForeignKey("dbo.Wallet", "UserID", "dbo.User");
            DropForeignKey("dbo.Transfer", "WalletToID", "dbo.Wallet");
            DropForeignKey("dbo.Transfer", "UserFromID", "dbo.User");
            DropForeignKey("dbo.DepositHistory", "DepositID", "dbo.Deposit");
            DropIndex("dbo.Transaction", new[] { "UserToID" });
            DropIndex("dbo.Transaction", new[] { "UserFromID" });
            DropIndex("dbo.Role", "RoleNameIndex");
            DropIndex("dbo.Log", new[] { "LogParentID" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.User", "UserNameIndex");
            DropIndex("dbo.Transfer", new[] { "WalletToID" });
            DropIndex("dbo.Transfer", new[] { "UserFromID" });
            DropIndex("dbo.Wallet", new[] { "UserID" });
            DropIndex("dbo.DepositHistory", new[] { "DepositID" });
            DropIndex("dbo.Deposit", new[] { "WalletID" });
            DropTable("dbo.Transaction");
            DropTable("dbo.Role");
            DropTable("dbo.Log");
            DropTable("dbo.UserRole");
            DropTable("dbo.UserLogin");
            DropTable("dbo.UserClaim");
            DropTable("dbo.User");
            DropTable("dbo.Transfer");
            DropTable("dbo.Wallet");
            DropTable("dbo.DepositHistory");
            DropTable("dbo.Deposit");
        }
    }
}
