﻿using DescomplicaBit.Application.Service.Interfaces;
using DescomplicaBit.Services.IoC;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SystemRoutine;

namespace SystemService
{
    public class Program
    {
        public static Container Container = new Container();

        public static void Main(string[] args)
        {

            Container.Options.DefaultScopedLifestyle = new ThreadScopedLifestyle();

            BootStrapper.RegisterServices(Container);

            Container.Verify();

            using (ThreadScopedLifestyle.BeginScope(Container))
            {
                var transferService = Container.GetInstance<ITransferService>();
                var depositService = Container.GetInstance<IDepositService>();

                var routine = new Routine(transferService, depositService);

                while (true)
                {
                    routine.Verification();
                    Thread.Sleep(300000);
                }
            }


        }
    }
}
