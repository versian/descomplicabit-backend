﻿using DescomplicaBit.Application.Service.Interfaces;
using DescomplicaBit.Domain.Enums;
using System;

namespace SystemRoutine
{
    public class Routine
    {
        public ITransferService TransferService { get; }
        public IDepositService DepositService { get; }

        public Routine(ITransferService transferService, IDepositService depositService)
        {
            TransferService = transferService;
            DepositService = depositService;
        }

        public void Verification()
        {
            try
            {
                Console.WriteLine("Verifying Deposits");
                VerifyDeposit();

                Console.WriteLine("Verifying Transfer");
                VerifyTransfer();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void VerifyDeposit()
        {
            try
            {
                var deposits = DepositService.GetDepositsToVerify();

                deposits.ForEach(deposit =>
                {
                    Console.WriteLine($"Deposit to address '{deposit.AddressToDeposit}' to wallet '{deposit.Wallet.Address}'");

                    var depositData = new { Balance = 1 }; // BitcoinService.GetBalance(deposit.AddressToDeposit);
                    // BitcoinService.TransferToPrincipalWallet(deposit.AddressToDeposit);

                    if (depositData.Balance > 0)
                        Console.WriteLine($"Balance Found: {depositData.Balance}");
                        
                    deposit.Wallet.Balance += depositData.Balance;
                    DepositService.InsertDepositHistory(deposit, depositData.Balance);

                    Console.WriteLine("Deposit Complete");
                });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void VerifyTransfer()
        {
            try
            {
                var transfers = TransferService.GetTransferToVerify(); // BitcoinService.GetBalance(deposit.AddressToDeposit);

                transfers.ForEach(transfer =>
                {
                    Console.WriteLine($"Transfer with status {transfer.Status.ToString()} found");
                    switch (transfer.Status)
                    {
                        case TransferStatus.SCHEDULED:
                            Console.WriteLine($"Transfering value of {transfer.Value} of {transfer.Cryptocurrency.ToString()}");
                            var transferID = "";// BitcoinService.Transfer(transfer.ExternalWallet, transfer.Value);
                            transfer.TransferID = transferID;
                            transfer.Status = TransferStatus.STARTED;
                            TransferService.UpdateTransfer(transfer);
                            Console.WriteLine("Transfer started with success");
                            break;

                        case TransferStatus.STARTED:
                            Console.WriteLine($"Verifying started transfer of {transfer.Value} of {transfer.Cryptocurrency.ToString()}");
                            var isComplete = true; // BitcoinService.GetTransferStatus(transfer.TransferID);

                            if (isComplete)
                                transfer.Status = TransferStatus.SUCCESS;

                            TransferService.UpdateTransfer(transfer);
                            Console.WriteLine("Transfer complete with success");
                            break;
                    }
                });
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
