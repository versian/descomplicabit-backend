﻿using DescomplicaBit.Application.Service.Interfaces;
using DescomplicaBit.Application.ViewModel;
using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using DescomplicaBit.Domain.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DescomplicaBit.Application.Service
{
    public class DepositService : IDepositService
    {
        public IRepository<Deposit> DepositRepository { get; }
        public IRepository<DepositHistory> DepositHistoryRepository { get; }

        public IUserService UserService { get; }
        public IWalletService WalletService { get; }


        public DepositService(IRepository<Deposit> depositRepository, IRepository<DepositHistory> depositHistoryRepository, IUserService userService, IWalletService walletService)
        {
            DepositRepository = depositRepository;
            DepositHistoryRepository = depositHistoryRepository;
            UserService = userService;
            WalletService = walletService;
        }


        public RequestReturnVM<DepositVM> PrepareWalletToDeposit(string currentUserID, Cryptocurrency cryptocurrency)
        {
            try
            {
                var userCriptoWallet = WalletService.CreateOrGetWallet(currentUserID, cryptocurrency);

                var createWallet = new { Address = "teste" }; // BitcoinService.CreateWallet()

                var depositExistent = DepositRepository.Find(wh => wh.WalletID == userCriptoWallet.ID);

                var deposit = depositExistent ?? DepositRepository.Insert(new Deposit
                {
                    Wallet = userCriptoWallet,
                    ExpirationDate = DateTime.Now.AddDays(1),
                    AddressToDeposit = createWallet.Address
                });

                if (deposit == null)
                    throw new Exception("Ocorreu um erro ao criar a carteira de depósito");

                return new RequestReturnVM<DepositVM>
                {
                    Success = true,
                    MessageTitle = "Depósito",
                    MessageBody = "Sua carteira de depósito foi criada com sucesso! Deposite as criptomoedas neste endereço abaixo antes da data de expiração para depositar suas criptomoedas no sistema",
                    Data = new DepositVM(deposit)
                };
            }
            catch (Exception e)
            {
                return new RequestReturnVM<DepositVM>
                {
                    Success = false,
                    MessageTitle = "Depósito",
                    MessageBody = e.Message,
                    Data = null
                };
            }
        }

        public RequestReturnVM<List<DepositHistoryVM>> GetDepositHistory(string currentUserID)
        {
            try
            {
                var result = DepositHistoryRepository.FindAll(wh => wh.Deposit.Wallet.UserID == currentUserID, i => i.Deposit, i => i.Deposit.Wallet, i => i.Deposit.Wallet.User).AsEnumerable()
                                                     .Select(s => new DepositHistoryVM(s))
                                                     .ToList();

                return new RequestReturnVM<List<DepositHistoryVM>>
                {
                    MessageTitle = "Histórico de Depósitos",
                    MessageBody = "Busca do histórico de depósitos feita com sucesso!",
                    Success = true,
                    Data = result
                };
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<Deposit> GetDepositsToVerify()
        {
            try
            {
                var deposits = DepositRepository.FindAll(wh => wh.ExpirationDate == null || wh.ExpirationDate >= DateTime.Now, i => i.Wallet).ToList();

                return deposits;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DepositHistory InsertDepositHistory(Deposit deposit, decimal value)
        {
            try
            {
                var depositHistory = new DepositHistory
                {
                    Deposit = deposit,
                    Status = DepositStatus.COMPLETED,
                    Value = value
                };

                return DepositHistoryRepository.Insert(depositHistory);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
