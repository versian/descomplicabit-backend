﻿using DescomplicaBit.Application.Service.Interfaces;
using DescomplicaBit.Application.Util.Functions;
using DescomplicaBit.Application.ViewModel;
using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using DescomplicaBit.Domain.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescomplicaBit.Application.Service
{
    public class TransactionService : ITransactionService
    {
        public IRepository<Transaction> TransactionRepository { get; }
        public IUserService UserService { get; }
        public IWalletService WalletService { get; }

        public TransactionService(IRepository<Transaction> transactionRepository, IUserService userService, IWalletService walletService)
        {
            TransactionRepository = transactionRepository;
            UserService = userService;
            WalletService = walletService;
        }


        public RequestReturnVM<TransactionVM> Create(string currentUserID, CreateTransactionVM createTransactionVM)
        {
            try
            {

                if (createTransactionVM.Value <= 0)
                    throw new Exception("O valor não pode ser menor ou igual a 0");

                var transaction = new Transaction();

                transaction.CopyPropertiesFrom(createTransactionVM);
                transaction.UserFromID = currentUserID;
                transaction.OperationCode = Functions.GenerateTransactionCode(currentUserID, transaction);


                switch(createTransactionVM.OperationType)
                {
                    case OperationType.DIRECT_OPERATION:

                        if (!Functions.IsWalletFormat(createTransactionVM.AccountTo))
                            throw new Exception("Formato de carteira inválida");

                        var userTo = UserService.GetUserByAccountID(createTransactionVM.AccountTo);

                        if (userTo == null)
                            throw new Exception("Conta inválida - Não existe um usuário com essa conta: " + createTransactionVM.AccountTo);

                        transaction.UserToID = userTo.Id;

                        break;

                    case OperationType.FAST_CODE:
                        transaction.OperationCode = Functions.GenerateFastCode(currentUserID, transaction);
                        break;

                    default:
                        break;
                }

                transaction.ExpirationDate = DateTime.Now.AddMinutes(3);

                var result = new TransactionVM(TransactionRepository.Insert(transaction), currentUserID);

                return new RequestReturnVM<TransactionVM>
                {
                    MessageTitle = "Criação de Venda",
                    MessageBody = "Transação criada com sucesso!",
                    Success = true,
                    Data = result
                };
            }
            catch (Exception e)
            {
                return new RequestReturnVM<TransactionVM>
                {
                    MessageTitle = "Criação de Venda",
                    MessageBody = "Ocorreu um erro ao criar a venda." + e.Message,
                    Success = false,
                    Data = null
                };
            }
        }

        public RequestReturnVM<bool> AcceptTransaction(string currentUserID, string operationCode)
        {
            try
            {
                var transaction = TransactionRepository.Find(wh => wh.OperationCode == operationCode, 
                                                                   i => i.UserFrom,
                                                                   i => i.UserFrom.Wallets,
                                                                   i => i.UserTo,
                                                                   i => i.UserTo.Wallets);

                var userTo = UserService.GetUserByID(currentUserID);

                if (transaction.OperationStatus != OperationStatus.ACTIVE)
                    throw new Exception("Essa transação não está disponível");

                if (transaction.OperationType == OperationType.DIRECT_OPERATION && transaction.UserToID != currentUserID)
                    throw new Exception("Operação inválida");

                if (transaction.UserFromID == currentUserID)
                    throw new Exception("Você não pode aceitar a própria operação");

                if (!WalletService.HasBalanceAvailable(currentUserID, transaction.Cryptocurrency, transaction.Value))
                    throw new Exception("Você não tem saldo suficiente");

                var userFromWallet = WalletService.CreateOrGetWalletByAccountID(transaction.UserFrom.AccountID, transaction.Cryptocurrency);
                var userToWallet = WalletService.CreateOrGetWalletByAccountID(userTo.AccountID, transaction.Cryptocurrency);

                if (!WalletService.ChangeBalances(userToWallet, userFromWallet, transaction.Value))
                    throw new Exception("Ocorreu um erro ao transferir os valores");

                transaction.OperationStatus = OperationStatus.SUCCESS;
                transaction.UserToID = currentUserID;

                TransactionRepository.Update(transaction);

                return new RequestReturnVM<bool>
                {
                    MessageTitle = "Compra",
                    MessageBody = "Compra realizada com sucesso!",
                    Success = true,
                    Data = true
                };
            }
            catch (Exception e)
            {
                return new RequestReturnVM<bool>
                {
                    MessageTitle = "Compra",
                    MessageBody = "Ocorreu um erro ao realizar a compra. " + e.Message,
                    Success = false,
                    Data = false
                };
            }
        }

        public RequestReturnVM<bool> CancelTransaction(string currentUserID, string operationCode)
        {
            try
            {
                var transaction = TransactionRepository.Find(wh => wh.OperationCode == operationCode);

                if (transaction == null)
                    throw new Exception("Transação inválida ou não existente");

                if (transaction.UserFromID != currentUserID || transaction.OperationStatus != OperationStatus.ACTIVE)
                    throw new Exception("Você não pode cancelar essa transação");

                transaction.OperationStatus = OperationStatus.CANCELED;

                TransactionRepository.Update(transaction);

                return new RequestReturnVM<bool>
                {
                    MessageTitle = "Cancelamento de Venda",
                    MessageBody = "Venda Cancelada com Sucesso!",
                    Success = true,
                    Data = true
                };
            }
            catch (Exception e)
            {
                return new RequestReturnVM<bool>
                {
                    MessageTitle = "Cancelamento de Venda",
                    MessageBody = "Ocorreu um erro ao cancelar a venda. " + e.Message,
                    Success = false,
                    Data = false
                };
            }
        }

        public RequestReturnVM<bool> RefuseTransaction(string currentUserID, string operationCode)
        {
            try
            {
                var transaction = TransactionRepository.Find(wh => wh.OperationCode == operationCode);

                if(transaction.OperationType == OperationType.DIRECT_OPERATION && transaction.UserToID == currentUserID)
                {
                    transaction.OperationStatus = OperationStatus.CANCELED;
                    TransactionRepository.Update(transaction);

                    return new RequestReturnVM<bool>
                    {
                        MessageTitle = "Recusar Transação",
                        MessageBody = "Transação recusada com sucesso!",
                        Success = true,
                        Data = true
                    };
                }

                throw new Exception("Você não pode recusar essa transação");
            }
            catch (Exception e)
            {
                return new RequestReturnVM<bool>
                {
                    MessageTitle = "Recusar Transação",
                    MessageBody = "Ocorreu um erro ao cancelar transação. " + e.Message,
                    Success = false,
                    Data = false
                };
            }
        }

        public RequestReturnVM<TransactionVM> GetTransaction(string currentUserID, string operationCode)
        {
            try
            {
                var currentUser = UserService.GetUserByID(currentUserID);
                var operation = TransactionRepository.Find(wh => wh.OperationCode == operationCode, i => i.UserFrom, i => i.UserTo);

                if (currentUser.Id != operation.UserFromID && currentUser.Id != operation.UserToID && operation.OperationType == OperationType.DIRECT_OPERATION)
                    throw new Exception("Voce nao tem permissao para visualizar essa transacao");

                return new RequestReturnVM<TransactionVM>
                {
                    MessageTitle = "Busca",
                    MessageBody = "Busca da transacao realizada com sucesso",
                    Success = true,
                    Data = new TransactionVM(operation, currentUserID)
                };
            }
            catch (Exception e)
            {
                return new RequestReturnVM<TransactionVM>
                {
                    MessageTitle = "Busca",
                    MessageBody = "Ocorreu um erro ao realizar a busca da transacao. " + e.Message,
                    Success = true,
                    Data = null
                };
            }
        }

        public RequestReturnVM<List<TransactionVM>> GetTransactions(string currentUserID)
        {
            try
            {
                var transactions = TransactionRepository.FindAll(wh => wh.UserFromID == currentUserID || wh.UserToID == currentUserID, i => i.UserFrom, i => i.UserTo).AsEnumerable().Select(s => new TransactionVM(s, currentUserID)).ToList();

                return new RequestReturnVM<List<TransactionVM>>
                {
                    MessageTitle = "Histórico de Transações",
                    MessageBody = "Histórico obtido com sucesso!",
                    Success = true,
                    Data = transactions
                };
            }
            catch (Exception e)
            {
                return new RequestReturnVM<List<TransactionVM>>
                {
                    MessageTitle = "Histórico de Transações",
                    MessageBody = "Ocorreu um erro ao obter as suas transações. " + e.Message,
                    Success = true,
                    Data = null
                };
            }
        }
    }
}
