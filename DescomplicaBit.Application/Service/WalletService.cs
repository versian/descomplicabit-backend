﻿using DescomplicaBit.Application.Service.Interfaces;
using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using DescomplicaBit.Domain.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DescomplicaBit.Application.Service
{
    public class WalletService : IWalletService
    {
        public IRepository<Wallet> WalletRepository { get; }
        public IUserService UserService { get; }

        public WalletService(IRepository<Wallet> walletRepository, IUserService userService)
        {
            WalletRepository = walletRepository;
            UserService = userService;
        }

        public Wallet CreateOrGetWallet(string userID, Cryptocurrency cryptocurrency)
        {
            try
            {
                var existentWallet = WalletRepository.Find(wh => wh.UserID == userID && wh.Active);

                if (existentWallet != null)
                    return existentWallet;

                var wallet = new Wallet
                {
                    Cryptocurrency = cryptocurrency,
                    UserID = userID,
                    Address = "teste" // BitcoinService.GetActiveWallet().Address
                };

                return WalletRepository.Insert(wallet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public Wallet CreateOrGetWalletByAccountID(string accountID, Cryptocurrency cryptocurrency)
        {
            try
            {
                if (accountID == null || accountID == "")
                    return null;

                var user = UserService.GetUserByAccountID(accountID);
                var existentWallet = WalletRepository.Find(wh => wh.UserID == user.Id && wh.Active);

                if (existentWallet != null)
                    return existentWallet;

                var wallet = new Wallet
                {
                    Cryptocurrency = cryptocurrency,
                    UserID = user.Id,
                    Address = "teste" // BitcoinService.GetActiveWallet().Address
                };

                return WalletRepository.Insert(wallet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ChangeBalances(Wallet walletFrom, Wallet walletTo, decimal amount)
        {
            try
            {
                walletFrom.Balance -= amount;
                walletTo.Balance += amount;

                WalletRepository.Update(walletTo);
                WalletRepository.Update(walletFrom);

                return true;
            }
            catch (Exception)
            {
                return false;
                throw;
            }
        }

        public void AddBalance(Wallet walletTo, decimal amount)
        {
            try
            {
                walletTo.Balance += amount;
                WalletRepository.Update(walletTo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool HasBalanceAvailable(string userID, Cryptocurrency cryptocurrency, decimal amount)
        {
            try
            {
                var wallet = CreateOrGetWallet(userID, cryptocurrency);

                return wallet.Balance >= amount;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Wallet> GetUserWallets(string userID)
        {
            try
            {
                var wallets = WalletRepository.FindAll(wh => wh.UserID == userID).ToList();

                return wallets;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
