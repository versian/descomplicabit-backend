﻿using DescomplicaBit.Application.Service.Interfaces;
using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using DescomplicaBit.Domain.Repositories.Interfaces;

namespace DescomplicaBit.Application.Service
{
    public class LogService : ILogService
    {
        public IRepository<Log> LogRepository { get; set; }

        public LogService(IRepository<Log> logRepository)
        {
            LogRepository = logRepository;
        }

        public Log SaveLog(Log log)
        {
            try
            {
                var result = LogRepository.Insert(log);

                return result;
            }
            catch
            {
                throw;
            }
        }

        public Log SaveLog(int logParentID, LogType logType, LogTitle logTitle, string description)
        {
            try
            {
                var log = new Log
                {
                    LogParentID = logParentID,
                    LogType = logType,
                    LogTitle = logTitle,
                    Description = description
                };

                return SaveLog(log);
            }
            catch
            {
                throw;
            }
        }

        public Log SaveLog(Log logParent, LogType logType, LogTitle logTitle, string description)
        {
            try
            {
                var log = new Log
                {
                    LogParent = logParent,
                    LogType = logType,
                    LogTitle = logTitle,
                    Description = description
                };

                return SaveLog(log);
            }
            catch
            {
                throw;
            }
        }
    }
}
