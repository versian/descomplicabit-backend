﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DescomplicaBit.Application.ViewModel;
using DescomplicaBit.Domain.Entities;

namespace DescomplicaBit.Application.Service.Interfaces
{
    public interface ITransactionService
    {
        RequestReturnVM<TransactionVM> Create(string currentUserID, CreateTransactionVM createTransactionVM);
        RequestReturnVM<bool> AcceptTransaction(string currentUserID, string operationCode);
        RequestReturnVM<bool> CancelTransaction(string currentUserID, string operationCode);
        RequestReturnVM<bool> RefuseTransaction(string currentUserID, string operationCode);
        RequestReturnVM<List<TransactionVM>> GetTransactions(string currentUserID);
        RequestReturnVM<TransactionVM> GetTransaction(string currentUserID, string operationCode);
    }
}
