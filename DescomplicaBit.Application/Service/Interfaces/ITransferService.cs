﻿using DescomplicaBit.Application.ViewModel;
using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using System.Collections.Generic;

namespace DescomplicaBit.Application.Service.Interfaces
{
    public interface ITransferService
    {
        RequestReturnVM<bool> ScheduleTransfer(string userID, ScheduleTransferVM scheduleTransferVM);
        RequestReturnVM<List<TransferVM>> GetTransferHistory(string userID);

        List<Transfer> GetTransferToVerify();
        void UpdateTransfer(Transfer transfer);
    }
}
