﻿using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;

namespace DescomplicaBit.Application.Service.Interfaces
{
    public interface ILogService
    {
        Log SaveLog(Log log);
        Log SaveLog(int logParentID, LogType logType, LogTitle logTitle, string description);
        Log SaveLog(Log logParent, LogType logType, LogTitle logTitle, string description);
    }
}
