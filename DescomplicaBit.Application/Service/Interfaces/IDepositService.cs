﻿using DescomplicaBit.Application.ViewModel;
using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using System.Collections.Generic;

namespace DescomplicaBit.Application.Service.Interfaces
{
    public interface IDepositService
    {
        RequestReturnVM<DepositVM> PrepareWalletToDeposit(string currentUserID, Cryptocurrency cryptocurrency);
        RequestReturnVM<List<DepositHistoryVM>> GetDepositHistory(string currentUserID);

        List<Deposit> GetDepositsToVerify();
        DepositHistory InsertDepositHistory(Deposit deposit, decimal value);
    }
}
