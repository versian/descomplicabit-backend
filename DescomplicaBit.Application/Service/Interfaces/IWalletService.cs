﻿using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using System.Collections.Generic;

namespace DescomplicaBit.Application.Service.Interfaces
{
    public interface IWalletService
    {
        Wallet CreateOrGetWallet(string userID, Cryptocurrency cryptocurrency);
        Wallet CreateOrGetWalletByAccountID(string accountID, Cryptocurrency cryptocurrency);
        bool ChangeBalances(Wallet walletFrom, Wallet walletTo, decimal amount);
        void AddBalance(Wallet walletTo, decimal amount);
        bool HasBalanceAvailable(string userID, Cryptocurrency cryptocurrency, decimal amount);
        List<Wallet> GetUserWallets(string userID);
    }
}
