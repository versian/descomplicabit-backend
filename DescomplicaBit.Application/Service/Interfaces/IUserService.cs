﻿using DescomplicaBit.Application.ViewModel;
using DescomplicaBit.Application.ViewModel.User;
using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using System.Threading.Tasks;

namespace DescomplicaBit.Application.Service.Interfaces
{
    public interface IUserService
    {
        Task<RequestReturnVM<bool>> RegisterAsync(RegisterVM registerVM);
        Task<RequestReturnVM<UserVM>> GetUserInfoAsync(string currentUserID);
        Task<RequestReturnVM<bool>> UpdatePasswordAsync(string currentUserID, UpdatePasswordVM updatePasswordVM);
        Task<RequestReturnVM<bool>> UpdateUserAsync(string currentUserID, UserVM userVM);
        RequestReturnVM<AccountInfoVM> GetAccountInfo(string currentUserID);

        User GetUserByAccountID(string accountID);
        User GetUserByID(string ID);
        bool ValidateConfirmationPassword(string currentUserID, string confirmationPassword);
    }
}
