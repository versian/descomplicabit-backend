﻿using DescomplicaBit.Application.Service.Interfaces;
using DescomplicaBit.Application.ViewModel;
using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using DescomplicaBit.Domain.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DescomplicaBit.Application.Service
{
    public class TransferService : ITransferService
    {
        public IRepository<Transfer> TransferRepository { get; }
        public IWalletService WalletService { get; }


        public TransferService(IRepository<Transfer> transferRepository, IWalletService walletService)
        {
            TransferRepository = transferRepository;
            WalletService = walletService;
        }


        public RequestReturnVM<bool> ScheduleTransfer(string userID, ScheduleTransferVM scheduleTransferVM)
        {
            try
            {
                if (scheduleTransferVM.Amount <= 0)
                    throw new Exception("O valor não pode ser menor ou igual a 0");

                string walletToID = null;

                if (!WalletService.HasBalanceAvailable(userID, scheduleTransferVM.Cryptocurrency, scheduleTransferVM.Amount))
                    throw new Exception("O usuário não tem saldo suficiente");

                var walletFrom = WalletService.CreateOrGetWallet(userID, scheduleTransferVM.Cryptocurrency);

                if(scheduleTransferVM.TransferType == TransferType.INTERNAL)
                {
                    var walletTo = WalletService.CreateOrGetWalletByAccountID(scheduleTransferVM.AddressID, scheduleTransferVM.Cryptocurrency);

                    if (!WalletService.ChangeBalances(walletFrom, walletTo, scheduleTransferVM.Amount))
                        throw new Exception("Ocorreu um erro ao trocar saldos");

                    walletToID = walletTo.ID;
                }

                var transfer = TransferRepository.Insert(new Transfer
                {
                    Cryptocurrency = scheduleTransferVM.Cryptocurrency,
                    TransferType = scheduleTransferVM.TransferType,
                    ExternalWallet = scheduleTransferVM.TransferType == TransferType.EXTERNAL_WALLET ? scheduleTransferVM.AddressID : null,
                    Status = scheduleTransferVM.TransferType == TransferType.EXTERNAL_WALLET ? TransferStatus.SCHEDULED : TransferStatus.SUCCESS,
                    UserFromID = userID,
                    Value = scheduleTransferVM.Amount,
                    WalletToID = walletToID
                });

                WalletService.AddBalance(walletFrom, -transfer.Value);

                return new RequestReturnVM<bool>
                {
                    Success = true,
                    MessageTitle = "Transferência",
                    MessageBody = $"Transferência {(scheduleTransferVM.TransferType == TransferType.EXTERNAL_WALLET ? "agendada" : "concluída")} com sucesso!",
                    Data = true
                };
            }
            catch (Exception e)
            {
                return new RequestReturnVM<bool>
                {
                    Success = false,
                    MessageTitle = "Transferência",
                    MessageBody = e.Message,
                    Data = true
                };
            }
        }

        public RequestReturnVM<List<TransferVM>> GetTransferHistory(string userID)
        {
            try
            {
                var result = TransferRepository.FindAll(wh => wh.UserFromID == userID).Select(s => new TransferVM(s)).ToList();

                return new RequestReturnVM<List<TransferVM>>
                {
                    MessageTitle = "Histórico de Transferência",
                    MessageBody = "Busca realizada com sucesso!",
                    Success = true,
                    Data = result
                };
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<Transfer> GetTransferToVerify()
        {
            try
            {
                var transactions = TransferRepository.FindAll(wh => wh.Status == TransferStatus.SCHEDULED && wh.Status == TransferStatus.STARTED && wh.TransferType == TransferType.EXTERNAL_WALLET).ToList();

                return transactions;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void UpdateTransfer(Transfer transfer)
        {
            try
            {
                TransferRepository.Update(transfer);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
