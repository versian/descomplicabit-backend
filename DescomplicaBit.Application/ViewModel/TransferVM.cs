﻿using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescomplicaBit.Application.ViewModel
{
    public class TransferVM
    {
        public TransferVM(Transfer t)
        {
            Cryptocurrency = t.Cryptocurrency;
            TransferType = t.TransferType;
            Status = t.Status;
            Value = t.Value;

            if (TransferType == TransferType.EXTERNAL_WALLET)
                AccountTo = t.ExternalWallet;
            else
                AccountTo = t.WalletTo.ID;
        }


        public Cryptocurrency Cryptocurrency { get; set; }

        public TransferType TransferType { get; set; }

        public TransferStatus Status { get; set; }

        public decimal Value { get; set; }

        public string AccountTo { get; set; }
    }
}
