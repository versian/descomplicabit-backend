﻿using DescomplicaBit.Domain.Entities;
using System;

namespace DescomplicaBit.Application.ViewModel
{
    public class DepositVM
    {
        public DepositVM(Deposit deposit)
        {
            AddressToDeposit = deposit.AddressToDeposit;
            ExpirationDate = deposit.ExpirationDate;
        }

        public string AddressToDeposit { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }
}
