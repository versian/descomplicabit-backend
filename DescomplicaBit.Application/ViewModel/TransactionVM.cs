﻿using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescomplicaBit.Application.ViewModel
{
    public class TransactionVM
    {
        public TransactionVM() { }

        public TransactionVM(Transaction t, string currentUserID)
        {
            Cryptocurrency = t.Cryptocurrency;
            OperationType = t.OperationType;
            OperationStatus = t.OperationStatus;
            OperationCode = t.OperationCode;
            Value = t.Value;
            AccountFrom = t.UserFrom?.AccountID;
            AccountTo = t.UserTo?.AccountID;
            CreateDate = t.CreationDate;
            ExpirationDate = t.ExpirationDate;
            TransactionDescription = t.TransactionDescription;

            IsPurchaseOperation = t.UserToID == currentUserID;
        }

        public Cryptocurrency Cryptocurrency { get; set; }

        public OperationType OperationType { get; set; }

        public OperationStatus OperationStatus { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string OperationCode { get; set; }

        public string TransactionDescription { get; set; }

        public decimal Value { get; set; }

        public string AccountFrom { get; set; }

        public string AccountTo { get; set; }


        public bool IsPurchaseOperation { get; set; }

        public bool IsExpired
        {
            get
            {
                return ExpirationDate <= DateTime.Now;
            }
        }
    }
}
