﻿using DescomplicaBit.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescomplicaBit.Application.ViewModel
{
    public class CreateTransactionVM
    {
        public Cryptocurrency Cryptocurrency { get; set; }

        public OperationType OperationType { get; set; }

        public string AccountTo { get; set; }

        public string TransactionDescription { get; set; }

        public decimal Value { get; set; }


        public OperationStatus OperationStatus
        {
            get
            {
                return OperationStatus.ACTIVE;
            }
        }
    }
}
