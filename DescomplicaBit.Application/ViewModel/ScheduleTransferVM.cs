﻿using DescomplicaBit.Domain.Enums;

namespace DescomplicaBit.Application.ViewModel
{
    public class ScheduleTransferVM
    {
        public Cryptocurrency Cryptocurrency { get; set; }

        public TransferType TransferType { get; set; }

        public string AddressID { get; set; }

        public decimal Amount { get; set; }
    }
}
