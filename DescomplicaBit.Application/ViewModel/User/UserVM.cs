﻿using DescomplicaBit.Domain.Enums;
using System;

namespace DescomplicaBit.Application.ViewModel.User
{
    public class UserVM
    {
        public string CPF { get; set; }

        public string RG { get; set; }

        public string CEP { get; set; }

        public string Address { get; set; }

        public string AddressNumber { get; set; }

        public string NeighborHood { get; set; }

        public string Complement { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string UserName { get; set; }

        public string AccountID { get; set; }

        public UserLevel Level { get; set; }

        public DateTime? JoinDate { get; set; }
    }
}
