﻿namespace DescomplicaBit.Application.ViewModel.User
{
    public class UpdatePasswordVM
    {
        public string OldPassword { get; set; }

        public string NewPassword { get; set; }
    }
}
