﻿using System.Collections.Generic;
using System.Linq;

namespace DescomplicaBit.Application.ViewModel.User
{
    public class AccountInfoVM
    {
        public AccountInfoVM(Domain.Entities.User user)
        {
            if (user == null)
                return;

            AccountID = user.AccountID;
            Wallets = user.Wallets.Select(s => new WalletVM(s)).ToList();
        }

        public string AccountID { get; set; }

        public List<WalletVM> Wallets { get; set; }
    }
}
