﻿using DescomplicaBit.Application.Util.Functions;
using DescomplicaBit.Domain.Enums;
using System;

namespace DescomplicaBit.Application.ViewModel
{
    public class RegisterVM
    {
        public string CPF { get; set; }

        public string RG { get; set; }

        public string CEP { get; set; }

        public DateTime Birthdate { get; set; }

        public string Address { get; set; }

        public string AddressNumber { get; set; }

        public string NeighborHood { get; set; }
        
        public string Complement { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string ConfirmationPassword { get; set; }

        public DateTime JoinDate
        {
            get
            {
                return DateTime.Now;
            }
        }

        public UserLevel Level
        {
            get
            {
                return UserLevel.COMMON_USER;
            }
        }

        public string AccountID
        {
            get
            {
                return Functions.GenerateWalletID();
            }
        }
    }
}
