﻿using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;

namespace DescomplicaBit.Application.ViewModel
{
    public class WalletVM
    {
        public WalletVM(Wallet s)
        {
            WalletID = s.ID;
            Cryptocurrency = s.Cryptocurrency;
            Balance = s.Balance;
        }

        public string WalletID { get; set; }

        public Cryptocurrency Cryptocurrency { get; set; }

        public decimal Balance { get; set; }
    }
}
