﻿using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DescomplicaBit.Application.ViewModel
{
    public class DepositHistoryVM
    {
        public DepositHistoryVM(DepositHistory d)
        {
            Cryptocurrency = d.Deposit.Wallet.Cryptocurrency;
            Status = d.Status;
            Value = d.Value;
            Date = d.CreationDate;
        }

        public Cryptocurrency Cryptocurrency { get; set; }

        public DepositStatus Status { get; set; }

        public decimal Value { get; set; }

        public DateTime Date { get; set; }
    }
}
