﻿using DescomplicaBit.Domain.Entities;
using Newtonsoft.Json;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace DescomplicaBit.Application.Util.Functions
{
    public static class Functions
    {
        public static TReturn ConvertObjectTo<TReturn>(object objectSource)
        {
            var result = JsonConvert.DeserializeObject<TReturn>(JsonConvert.SerializeObject(objectSource));

            return result;
        }

        public static TReturn CopyPropertiesFrom<TReturn>(this TReturn self, object parent)
        {
            var fromProperties = parent.GetType().GetProperties();
            var toProperties = self.GetType().GetProperties();

            foreach (var fromProperty in fromProperties)

                foreach (var toProperty in toProperties)

                    if (fromProperty.Name == toProperty.Name && fromProperty.PropertyType == toProperty.PropertyType && fromProperty.GetValue(parent) != null)
                    {
                        toProperty.SetValue(self, fromProperty.GetValue(parent));
                        break;
                    }

            return self;
        }

        public static bool IsWalletFormat(this string text) => !(text == null || text.Length == 0 || !Regex.IsMatch(text, "\\d\\d\\d\\d\\d\\d\\d-\\d\\d"));

        public static string GenerateTransactionCode(string currentUserID, Transaction transaction)
        {
            using (SHA256 sha = new SHA256Managed())
            {
                var data = $"{currentUserID} - {transaction.CreationDate.Ticks} - {transaction.Cryptocurrency} - {transaction.Value} - {transaction.OperationType} - {transaction.UserToID}";
                var hashBytes = sha.ComputeHash(Encoding.UTF8.GetBytes(data));
                var hash = Convert.ToBase64String(hashBytes);

                return hash;
            }
        }

        public static string GenerateFastCode(string currentUserID, Transaction transaction)
        {
            var code = GenerateTransactionCode(currentUserID, transaction).GetHashCode();
            var fastCode = ConvertToHex(Math.Abs(code));

            return fastCode;
        }

        public static string ConvertToHex(int number)
        {
            var result = number.ToString("X");

            return result;
        }

        public static int ConvertFromHex(string hex)
        {
            var result = Convert.ToInt32(hex, 16);

            return result;
        }

        public static string GenerateWalletID()
        {
            return Math.Abs(DateTime.Now.Ticks.GetHashCode()).ToString("#-##").PadLeft(11, '1');
        }
    }
}
