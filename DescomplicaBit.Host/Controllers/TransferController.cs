﻿using DescomplicaBit.Application.Service.Interfaces;
using DescomplicaBit.Application.ViewModel;
using DescomplicaBit.Host.Controllers.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace DescomplicaBit.Host.Controllers
{
    [Authorize, RoutePrefix("Transfer")]
    public class TransferController : BaseController
    {
        public ITransferService TransferService { get; }

        public TransferController(ITransferService transferService)
        {
            TransferService = transferService;
        }

        [HttpPost]
        [Route(nameof(ScheduleTransfer))]
        public IHttpActionResult ScheduleTransfer(ScheduleTransferVM scheduleTransferVM)
        {
            try
            {
                var result = TransferService.ScheduleTransfer(CurrentUserID, scheduleTransferVM);

                return DoResponse(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route(nameof(GetTransferHistory))]
        public IHttpActionResult GetTransferHistory()
        {
            try
            {
                var result = TransferService.GetTransferHistory(CurrentUserID);

                return DoResponse(result);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}