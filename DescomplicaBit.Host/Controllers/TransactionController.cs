﻿using DescomplicaBit.Application.Service.Interfaces;
using DescomplicaBit.Application.ViewModel;
using DescomplicaBit.Host.Controllers.Base;
using System;
using System.Web.Http;

namespace DescomplicaBit.Host.Controllers
{
    [Authorize, RoutePrefix("Transaction")]
    public class TransactionController : BaseController
    {
        public ITransactionService TransactionService { get; }
        public IUserService UserService { get; }

        public TransactionController(ITransactionService transactionService, IUserService userService)
        {
            TransactionService = transactionService;
            UserService = userService;
        }

        [HttpPost]
        [Route(nameof(Create))]
        public IHttpActionResult Create(CreateTransactionVM createTransactionVM)
        {
            try
            {
                var result = TransactionService.Create(CurrentUserID, createTransactionVM);

                return DoResponse(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route(nameof(AcceptTransaction))]
        public IHttpActionResult AcceptTransaction(string operationCode, string confirmationPwd)
        {
            try
            {
                if (!UserService.ValidateConfirmationPassword(CurrentUserID, confirmationPwd))
                    return Unauthorized();

                var result = TransactionService.AcceptTransaction(CurrentUserID, operationCode);

                return DoResponse(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route(nameof(CancelTransaction))]
        public IHttpActionResult CancelTransaction(string operationCode, string confirmationPwd)
        {
            try
            {
                if (!UserService.ValidateConfirmationPassword(CurrentUserID, confirmationPwd))
                    return Unauthorized();

                var result = TransactionService.CancelTransaction(CurrentUserID, operationCode);

                return DoResponse(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route(nameof(RefuseTransaction))]
        public IHttpActionResult RefuseTransaction(string operationCode, string confirmationPwd)
        {
            try
            {
                if (!UserService.ValidateConfirmationPassword(CurrentUserID, confirmationPwd))
                    return Unauthorized();

                var result = TransactionService.RefuseTransaction(CurrentUserID, operationCode);

                return DoResponse(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route(nameof(GetTransaction))]
        public IHttpActionResult GetTransaction(string operationCode)
        {
            try
            {
                var result = TransactionService.GetTransaction(CurrentUserID, operationCode);

                return DoResponse(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route(nameof(GetTransactions))]
        public IHttpActionResult GetTransactions()
        {
            try
            {
                var result = TransactionService.GetTransactions(CurrentUserID);

                return DoResponse(result);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}