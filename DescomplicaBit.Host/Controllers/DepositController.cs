﻿using DescomplicaBit.Application.Service.Interfaces;
using DescomplicaBit.Domain.Enums;
using DescomplicaBit.Host.Controllers.Base;
using System;
using System.Web.Http;

namespace DescomplicaBit.Host.Controllers
{
    [Authorize, RoutePrefix("Deposit")]
    public class DepositController : BaseController
    {
        public IDepositService DepositService { get; }

        public DepositController(IDepositService depositService)
        {
            DepositService = depositService;
        }

        [HttpPost]
        [Route(nameof(CreateDeposit) + "/{cryptocurrency}")]
        public IHttpActionResult CreateDeposit(Cryptocurrency cryptocurrency)
        {
            try
            {
                var result = DepositService.PrepareWalletToDeposit(CurrentUserID, cryptocurrency);

                return DoResponse(result);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route(nameof(GetDepositHistory))]
        public IHttpActionResult GetDepositHistory()
        {
            try
            {
                var result = DepositService.GetDepositHistory(CurrentUserID);

                return DoResponse(result);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}