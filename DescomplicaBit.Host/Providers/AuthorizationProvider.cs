﻿using DescomplicaBit.Application.Service;
using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Enums;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DescomplicaBit.Host.Providers
{
    public class AuthorizationProvider : OAuthAuthorizationServerProvider
    {
        public AuthorizationProvider()
        {
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            const string allowedOrigin = "*";

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var userManager = Startup.Container.GetInstance<UserService>();

            var user = await userManager.FindAsync(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "Usuário ou Senha Incorreta");
                return;
            }

            var oAuthIdentity = await userManager.CreateIdentityAsync(user, "Bearer");
            oAuthIdentity.AddClaim(new Claim(ClaimTypes.Role, user.Level.ToString()));

            var ticket = new AuthenticationTicket(oAuthIdentity, null);

            context.Validated(ticket);
        }
    }
}