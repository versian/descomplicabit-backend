﻿using DescomplicaBit.Host.Controllers;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Test.Controllers
{
    public class UserControllerTest
    {
        [Fact]
        public void AuthTest()
        {
            Assert.Equal(5, 5);
        }
        
        [Fact]
        public void RegisterTest()
        {
            Assert.Equal(5, 5);
        }

        [Fact]
        public async void UpdatePasswordTest()
        {
            Assert.Equal(56, 56);
        }
    }
}
