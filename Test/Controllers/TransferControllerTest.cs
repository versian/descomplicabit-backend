﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Test.Controllers
{
    public class TransferControllerTest
    {
        [Fact]
        public void ScheduleTransferTest()
        {
            Assert.Equal(5, 5);
        }

        [Fact]
        public void GetTransferHistoryTest()
        {
            Assert.Equal(5, 5);
        }
    }
}
