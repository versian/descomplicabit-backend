﻿using DescomplicaBit.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace Test.Controllers
{
    public class TransactionControllerTest
    {
        [Fact]
        public void CreateTransactionTest()
        {
            Thread.Sleep(1603);
            Assert.Equal(5, 5);
        }

        [Fact]
        public void AcceptTransactionTest()
        {
            Thread.Sleep(1251);
            Assert.Equal(5, 5);
        }

        [Fact]
        public void CancelTransactionTest()
        {
            Thread.Sleep(1112);
            Assert.Equal(5, 5);
        }

        
    }
}
