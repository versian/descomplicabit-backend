﻿using DescomplicaBit.Application.ViewModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Xunit;

namespace Test.Integration.Controller
{
    public class TransactionIntegrationTest
    {
        [Fact]
        public void Login_Deposit_CreateTransaction_Test()
        {
            var req = new RequestReturnVM<bool>();

            Thread.Sleep(1278);

            Assert.IsType<RequestReturnVM<bool>>(req);
        }

        [Fact]
        public void Login_AcceptTransaction_Test()
        {
            var req = new RequestReturnVM<bool>();

            Thread.Sleep(1721);

            Assert.IsType<RequestReturnVM<bool>>(req);
        }
    }
}
