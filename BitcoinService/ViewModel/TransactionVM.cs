﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinService.ViewModel
{
    public class TransactionVM
    {

        public string TXID { get; set; }

        public int Confirmations { get; set; }


        public bool IsConfirmed
        {
            get
            {
                return Confirmations > 3;
            }
        }

    }
}
