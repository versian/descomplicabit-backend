﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinService.ViewModel.TransactionInfo
{
    public class GetFeeTransactionDataVM
    {

        public string Estimated_Network_Fee { get; set; }

    }
}
