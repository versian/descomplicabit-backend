﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinService.ViewModel.TransactionInfo
{
    public class GetFeeTransactionVM
    {

        public string Status { get; set; }

        public GetFeeTransactionDataVM Data { get; set; }

    }
}
