﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinService.ViewModel.Transfer
{
    public class TransferVM
    {

        public string Status { get; set; }

        public TransferDataVM Data { get; set; }

    }
}
