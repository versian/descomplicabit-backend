﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinService.ViewModel
{
    public class CreateWalletVM
    {

        public string PublicKey { get; set; }

        public string PrivateKey { get; set; }

    }
}
