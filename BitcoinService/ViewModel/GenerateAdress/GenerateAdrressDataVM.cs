﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BitcoinService.ViewModel
{
    public class GenerateAdrressDataVM
    {

        public string Network { get; set; }

        public string User_id { get; set; }

        public string Address { get; set; }

        public string Label { get; set; }

    }
}