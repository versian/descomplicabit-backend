﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace BitcoinService.ViewModel.AddressInfo
{
    public class GetAddressByHashVM
    {

        public string Status { get; set; }

        public GetAddressByHashDataVM Data { get; set; }

    }
}