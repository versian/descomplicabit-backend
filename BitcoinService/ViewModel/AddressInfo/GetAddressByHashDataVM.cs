﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinService.ViewModel.AddressInfo
{
    public class GetAddressByHashDataVM
    {

        public string Network { get; set; }

        public string User_id { get; set; }

        public string Address { get; set; }

        public string Label { get; set; }

        public string Available_Balance { get; set; }

        public string Pending_Received_Balance { get; set; }

    }
}
