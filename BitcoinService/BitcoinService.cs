﻿using BitcoinService.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BitcoinRPC
{
    public class BitcoinService
    {
        
        private static readonly string workingPath = "C:\\Users\\Olivio\\Documents\\bitcoin-0.16.2\\bin\\bitcoin-cli.exe";
        private static readonly string principalWallet = ConfigurationManager.AppSettings["PrincipalWallet"];


        public static GetDifficultyVM GetDifficulty()
        {
            string argument = "getdifficulty";

            var resultString = RPC(argument);
            
            var objectReturn = new GetDifficultyVM
            {
                Difficulty = Convert.ToDouble(resultString)
            };

            return objectReturn;
        }

       
        public static TransactionVM GetRawTransaction(string txid)
        {

            string argument = "getrawtransaction " + txid + " true";

            var resultString = RPC(argument);
            
            var objectReturn = JsonConvert.DeserializeObject<TransactionVM>(resultString);

            return objectReturn;
        }

        public static GetWalletInfoVM GetBalance(string account)
        {

            string argument = "getbalance " + account + " 3";

            var resultString = RPC(argument);

            var objectReturn = JsonConvert.DeserializeObject<GetWalletInfoVM>(resultString);

            return objectReturn;
        }

        public static CreateWalletVM CreateWallet()
        {

            var firstArgument = "getnewaddress";

            var resultFirstString = RPC(firstArgument);

            var secondArgument = "dumpprivkey " + resultFirstString;

            var resultSecondString = RPC(secondArgument);

            var objectReturn = new CreateWalletVM
            {
                PublicKey = resultFirstString,
                PrivateKey = resultSecondString
            };

            return objectReturn;
        }

        public static void TransferAccountToPrincipal(string accountID)
        {
            var amount = GetBalance(accountID).Balance;

            var argument = "sendtoaddress " + principalWallet + " " + amount;

            var resultString = RPC(argument);
        }

        private static string RPC(string argument)
        {
            var result = "";

            using (var process = new Process())
            {
                process.StartInfo.FileName = workingPath;
                process.StartInfo.Arguments = argument;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.StartInfo.RedirectStandardError = true;
                process.OutputDataReceived += (s, e) => result += e.Data + "\n";
                process.ErrorDataReceived += (s, e) => result += e.Data + "\n";
                process.Start();
                process.BeginOutputReadLine();
                process.BeginErrorReadLine();
                process.WaitForExit();

                return result;
            }
        }
    }
}
