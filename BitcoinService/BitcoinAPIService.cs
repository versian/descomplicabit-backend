﻿using BitcoinService.ViewModel;
using BitcoinService.ViewModel.AddressInfo;
using BitcoinService.ViewModel.TransactionInfo;
using BitcoinService.ViewModel.Transfer;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Text;

namespace BitcoinService
{
    public class BitcoinAPIService
    {

        private static readonly string API_URL = "https://block.io/api/v2";
        private static readonly string API_KEY = "aca3-f4b2-a917-7317";
        private static readonly string PIN_KEY = "v3rsianprojetos123";


        public static GenerateAddressApiVM GenerateAdress(string label)
        {
            try
            {
                var _url = API_URL + "/get_new_address/?api_key=" + API_KEY + "&label=" + label;

                var resultString = CreateRequest("GET", _url, null);

                GenerateAddressApiVM objectReturn = JsonConvert.DeserializeObject<GenerateAddressApiVM>(resultString);

                return objectReturn;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static GetAddressByHashVM GetBalanceAddressByHash(string account)
        {

            try
            {
                var _url = API_URL + "/get_address_balance/?api_key=" + API_KEY + "&addresses=" + account;

                var resultString = CreateRequest("GET", _url, null);

                GetAddressByHashVM objectReturn = JsonConvert.DeserializeObject<GetAddressByHashVM>(resultString);

                return objectReturn;
            }
            catch (Exception)
            {
                throw;
            }

        }


        public static GetRawTransactionVM GetTransactionByTXID(string txid)
        {
            try
            {

                var _url = API_URL + "/get_raw_transaction/?api_key=" + API_KEY + "&txid=" + txid;

                var resultString = CreateRequest("GET", _url, null);

                GetRawTransactionVM objectReturn = JsonConvert.DeserializeObject<GetRawTransactionVM>(resultString);

                return objectReturn;

            }
            catch (Exception)
            {
                throw;
            }
        }


        public static TransferVM Transfer(string fromTXID, string toTXID, double amount)
        {
            try
            {
                //FINAL VALUE WITH FEE'S.
                var finalValueConverter = GetFinalValueWithFee(toTXID, amount);

                var stringValue = ConvertDecimal(finalValueConverter);

                //URL TRANSFER TO PRINCIPAL ACCOUNT - DEFAULT.
                var _url = API_URL + "/withdraw_from_addresses/?api_key=" + API_KEY + "&from_addresses=" + fromTXID +
                "&to_addresses=" + toTXID + "&amounts=" + stringValue + "&pin=" + PIN_KEY;

                var resultString = CreateRequest("GET", _url, null);

                TransferVM objectReturn = JsonConvert.DeserializeObject<TransferVM>(resultString);

                return objectReturn;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public static decimal GetFinalValueWithFee(string toTXID, double amount)
        {

            var value = Convert.ToDecimal(ConvertDouble(amount, true));

            var _urlFees = API_URL + "/get_network_fee_estimate?api_key=" + API_KEY + "&amounts=" + value + "&to_addresses=" + toTXID;

            var resultFeeString = CreateRequest("GET", _urlFees, null);

            GetFeeTransactionVM objectFee = JsonConvert.DeserializeObject<GetFeeTransactionVM>(resultFeeString);

            var feeInDouble = Convert.ToDouble(objectFee.Data.Estimated_Network_Fee.ToString().Replace(".", ","));

            var finalValue = Convert.ToDecimal(amount - feeInDouble);

            return finalValue;

        }

        public static string ConvertDouble(double amount, Boolean commaToDot)
        {

            if (commaToDot == true)
            {
                var value = amount.ToString().Replace(",", ".");

                return value;
            }
            else
            {
                var value = amount.ToString().Replace(".", ",");

                return value;
            }
        }


        public static string ConvertDecimal(decimal amount)
        {

            var value = amount.ToString().Replace(",", ".");

            return value;

        }


        private static string CreateRequest(string method, string url, object data)
        {
            try
            {
                string _response = "";

                var _url = url;

                byte[] _data = null;

                if (method != "GET")
                    _data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(data));

                using (var client = new WebClient())
                {
                    client.Headers.Add("Content-Type", "application/json");
                    client.Headers.Add("Cache-Control", "no-cache");

                    if (method.Equals("POST"))
                    {
                        byte[] byteResult = client.UploadData(_url, method, _data);
                        _response = Encoding.UTF8.GetString(byteResult);
                    }
                    else
                        _response = client.DownloadString(url);

                    return _response;
                }
            }
            catch (WebException ex)
            {
                using (StreamReader Reader = new StreamReader(ex.Response.GetResponseStream()))
                {
                    var result = Reader.ReadToEnd();

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }

}
