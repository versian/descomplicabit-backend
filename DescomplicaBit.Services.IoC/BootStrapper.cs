﻿using DescomplicaBit.Application.Service;
using DescomplicaBit.Application.Service.Interfaces;
using DescomplicaBit.Domain.Context;
using DescomplicaBit.Domain.Entities;
using DescomplicaBit.Domain.Repositories;
using DescomplicaBit.Domain.Repositories.Interfaces;
using SimpleInjector;

namespace DescomplicaBit.Services.IoC
{
    public class BootStrapper
    {
        public static void RegisterServices(Container container)
        {
            #region "Context"

            container.Register<DescomplicaBitContext>(Lifestyle.Scoped);

            #endregion

            #region Inject Services

            container.Register<ILogService, LogService>(Lifestyle.Scoped);
            container.Register<IUserService, UserService>(Lifestyle.Scoped);
            container.Register<ITransferService, TransferService>(Lifestyle.Scoped);
            container.Register<IWalletService, WalletService>(Lifestyle.Scoped);
            container.Register<IDepositService, DepositService>(Lifestyle.Scoped);
            container.Register<ITransactionService, TransactionService>(Lifestyle.Scoped);

            #endregion

            #region Inject Repositories

            container.Register<IRepository<Log>, Repository<Log>>(Lifestyle.Scoped);
            container.Register<IRepository<User>, Repository<User>>(Lifestyle.Scoped);
            container.Register<IRepository<Transfer>, Repository<Transfer>>(Lifestyle.Scoped);
            container.Register<IRepository<Deposit>, Repository<Deposit>>(Lifestyle.Scoped);
            container.Register<IRepository<DepositHistory>, Repository<DepositHistory>>(Lifestyle.Scoped);
            container.Register<IRepository<Wallet>, Repository<Wallet>>(Lifestyle.Scoped);
            container.Register<IRepository<Transaction>, Repository<Transaction>>(Lifestyle.Scoped);

            #endregion

        }
    }
}
